# Regret matching for network congestion games

To see the simulation, just run ```main.py```.
How to calibrate the simulation : 
* `list_src_dst`: list of tuples representing players with source and destination node
* `nb_actions`: number of possible paths per player

You can also change some parameters in the module `game.py`:
* `self.prob_actions`: change the initialization of the vector to observe different behaviours
* `max_K`: change the density threshold for congestion

If the animation does not run, there might be a problem with a subpackage of `networkx`, try to execute :
```pip install decorator == 5.0.7 --user```

`Game Strategies` is the notebook for all the first 3 games and algorithms.