import numpy as np
from get_path import get_paths

class regretMatchingPlayer:

    def __init__(self, id, graph, src, dst, nb_actions, nb_iterations, manual_actions = []):
        """
        id: integer indentifying the player
        graph: networkx graph representing the roads' network
        actions: list of the player's possible paths, determined by the function get_paths 
        nb_iterations: number of iterations of the game
        """
        self.id = id
        self.actions = get_paths(graph, src, dst, nb_actions)
        self.nb_actions = nb_actions
        if not manual_actions == []:
            self.actions = manual_actions
            self.nb_actions = len(manual_actions)

        self.ind_decisions = np.empty(nb_iterations,dtype=np.int8)  # Indices of the decisions vector
        self.U = np.zeros(nb_iterations)                            # Utility vector
        self.V = np.zeros(self.nb_actions)                          # Averate payoff for each action vector
        self.R = np.zeros(self.nb_actions)                          # Cumulative regret vector
        self.sigma = np.zeros((nb_iterations+1, self.nb_actions))   # Probabilities distribution for the player's strategies
        prob_shortest = 0.6
        self.sigma[0] = [prob_shortest] + [(1-prob_shortest)/(self.nb_actions-1) for i in range(self.nb_actions-1)]

        # print("User n°", self.id, "\nPaths:", self.actions)

    def decision(self,T):
        self.ind_decisions[T] = np.random.choice(self.nb_actions, p = self.sigma[T])
        return self.actions[self.ind_decisions[T]]

    def play(self, T, dynamic_adjacency_tensor):
        ## Update U
        self.chosen_path = self.actions[self.ind_decisions[T]]
        self.U[T] = compute_utility(dynamic_adjacency_tensor[T], self.chosen_path)
        ## Update V, then R
        for k in range(self.nb_actions):
            # self.V[k] = np.mean(np.array([compute_utility(dynamic_adjacency_tensor[t], self.actions[k]) for t in range(T+1)]))
            if T == 0:
                self.V[k] = compute_utility(dynamic_adjacency_tensor[T], self.actions[k])/(T-1)
            elif T == 1:
                self.V[k] = (self.V[k] + compute_utility(dynamic_adjacency_tensor[T], self.actions[k]))/2
            else:
                self.V[k] = ((T-1)/T)*(self.V[k] + compute_utility(dynamic_adjacency_tensor[T], self.actions[k])/(T-1))
            self.R[k] = max(0, self.V[k] - np.mean(self.U[:T+1]))

        if np.sum(self.R) == 0:
            print(self.id, "Error")
            print("U", self.U[0])
            print("V", self.V)
            print("R", self.R)

        ## Update sigma
        self.sigma[T+1] = self.R/np.sum(self.R)


def compute_utility(dynamic_adjacency_matrix, path):
    """dynamic_adjacency_matrix : V * V size matrix containing the actualized weights"""
    edges = zip(path,path[1:])
    cost = 0
    for edge in edges:
        cost += dynamic_adjacency_matrix[edge[0]][edge[1]]
    return cost    


def actualize_busyness_matrix(busyness_matrix_coefficient):
    return busyness_matrix_coefficient + 1


def actualize_dynamic_matrix(adjacency_matrix,busyness_matrix,len_graph,nb_users):
    cost_matrix = adjacency_matrix.apply(lambda x: np.zeros(len_graph))   # Time to cross the edge matrix
    unconstrained_v = 20                                                  # Velocity if no traffic jam
    alpha = 1                                                             # Controls the slope of the decreasing exponential

    for i in range(1, len_graph + 1):
        for j in range(1, len_graph + 1):
            K = busyness_matrix[i][j]/adjacency_matrix[i][j]
            max_K = 10 # Saturation if 10 vehicles per kilometer
            if K < max_K:
                v = unconstrained_v
            else:
                v = unconstrained_v/(K - max_K +1)**alpha
            cost_matrix[i][j] = adjacency_matrix[i][j]/v
    
    return - cost_matrix


def create_cluster(nbr_people, nbr_departure_cluster, nbr_arrival_cluster, var, len_graph):

    # clusters_departure = list(np.random.randint(1, len_graph, size=nbr_departure_cluster))
    # clusters_arrival = list(np.random.randint(1, len_graph, size=nbr_arrival_cluster))
    clusters_departure = list(np.random.randint(2, 5, size=nbr_departure_cluster))
    clusters_arrival = list(np.random.randint(8, 11, size=nbr_arrival_cluster))
    journeys = []

    for i in range(nbr_people):
        possible_departures = clusters_departure + list(np.random.randint(1, len_graph, size=var))
        possible_arrivals = clusters_arrival + list(np.random.randint(1, len_graph, size=var))
        departure = possible_departures[np.random.randint(nbr_departure_cluster + var)]
        arrival = possible_arrivals[np.random.randint(nbr_arrival_cluster + var)]
        journeys.append((departure, arrival))
    
    print(f'Clusters successfully created !')
    return journeys

