import networkx as nx
from graph import *
import random


def shortest_path(G,src,dst):
    """For nx graphs"""
    path = nx.shortest_path(G, source=src, target=dst)
    return path


def get_paths(graph, src, dst, max_number_paths):
    shortest = shortest_path(graph, src, dst)
    number_paths = 1
    iter = 1
    removed = []
    paths = [[shortest,[]]]
    while number_paths < max_number_paths:
        try :
            tmp = random.choice(paths) # Randomize the node to remove
            path_to_change, removed_path = tmp[0], tmp[1]
            if not path_to_change[1:-1] == []:
                node_to_remove = random.choice(path_to_change[1:-1])
                if removed_path + [node_to_remove] not in removed:
                    removed.append(removed_path + [node_to_remove])
                    tmp_graph = graph.copy()
                    for node in removed_path + [node_to_remove]:
                        tmp_graph = remove_node(tmp_graph,node)
                    path = shortest_path(tmp_graph, src, dst)
                    if path not in paths:
                        paths.append([path,removed_path + [node_to_remove]])
                        number_paths += 1
        except nx.exception.NetworkXNoPath:
            pass
    paths = [path[0] for path in paths]
    return paths


def main():
    graph = grands_axes_paris.copy()
    src = 6
    dst = 11
    max_number_paths = 5
    paths = get_paths(graph, src, dst, max_number_paths)

    ## Draw the paths created by get_paths

    fig = plt.figure(figsize=(8,8))
    seed = 0 
    pos = nx.spring_layout(grands_axes_paris, seed = seed)
    nx.draw_networkx(grands_axes_paris, pos)
    colors = ["b",'r','g','m','c']
    width = [20,16,12,8,4]

    for ID,path in enumerate(paths):
        path_edges = [[path[j],path[j+1]] for j in range(len(path)-1)]
        nx.draw_networkx_edges(grands_axes_paris,pos,edgelist=path_edges,edge_color=colors[ID],width=width[ID])

    plt.draw()
    plt.show()


if __name__ == "__main__":
    main()