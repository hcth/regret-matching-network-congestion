import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

grands_axes_paris_dict = {1: [{'node': 2, 'weight': 7}, {'node': 13, 'weight': 3}, {'node': 14, 'weight': 5}], 
                     2: [{'node': 1, 'weight': 7}, {'node': 3, 'weight': 8}, {'node': 25, 'weight': 4}],
                     3: [{'node': 2, 'weight': 8}, {'node': 4, 'weight': 7}, {'node': 24, 'weight': 4}], 
                     4: [{'node': 3, 'weight': 7}, {'node': 5, 'weight': 5}, {'node': 22, 'weight': 7}], 
                     5: [{'node': 4, 'weight': 5}, {'node': 6, 'weight':1}, {'node': 20, 'weight': 2}], 
                     6: [{'node': 5, 'weight': 1}, {'node': 7, 'weight': 6}, {'node': 19, 'weight': 2}], 
                     7: [{'node': 6, 'weight': 6}, {'node': 8, 'weight': 1}, {'node': 18, 'weight': 3}], 
                     8: [{'node': 7, 'weight': 1}, {'node': 9, 'weight': 4}], 
                     9: [{'node': 8, 'weight': 4}, {'node': 10, 'weight': 4}, {'node': 17, 'weight': 3}], 
                     10: [{'node': 9, 'weight': 4}, {'node': 11,'weight': 2}, {'node': 16, 'weight': 4}], 
                     11: [{'node': 10, 'weight': 2}, {'node': 12, 'weight': 1}], 
                     12: [{'node': 11, 'weight': 1}, {'node': 13, 'weight': 1}, {'node': 15, 'weight': 3}],
                     13: [{'node': 12, 'weight': 1}, {'node': 1, 'weight': 3}, {'node': 14, 'weight': 3}], 
                     14: [{'node': 1, 'weight': 5}, {'node': 13, 'weight': 3}, {'node': 15, 'weight':1}, {'node': 26, 'weight':4}], 
                     15: [{'node': 12, 'weight': 3}, {'node': 14, 'weight': 1}, {'node': 27, 'weight': 4}, {'node': 16, 'weight': 3}], 
                     16: [{'node': 10, 'weight': 4}, {'node': 15, 'weight': 3}, {'node': 27, 'weight': 4}, {'node': 17, 'weight': 3}],
                     17: [{'node': 16, 'weight': 3}, {'node': 9, 'weight': 3}, {'node': 18, 'weight': 5}], 
                     18: [{'node': 17, 'weight': 5}, {'node': 7, 'weight': 3}, {'node': 19, 'weight': 5}, {'node': 21, 'weight': 3}], 
                     19: [{'node': 18, 'weight': 5}, {'node': 6, 'weight': 2}, {'node': 20, 'weight': 1}, {'node': 21, 'weight': 5}], 
                     20: [{'node': 19, 'weight': 1}, {'node': 5, 'weight': 2}, {'node': 22, 'weight': 5}],
                     21: [{'node': 18, 'weight': 3}, {'node': 19, 'weight': 5}, {'node': 22, 'weight': 1}, {'node': 27, 'weight': 5}],
                     22: [{'node': 21, 'weight': 1}, {'node': 20, 'weight': 5}, {'node': 23, 'weight': 1}, {'node': 26, 'weight': 5}],
                     23: [{'node': 22, 'weight': 1}, {'node': 4, 'weight': 7}, {'node': 24, 'weight': 3}], 
                     24: [{'node': 23, 'weight': 3}, {'node': 3, 'weight': 4}, {'node': 25, 'weight': 6}], 
                     25: [{'node': 24, 'weight': 6}, {'node': 2, 'weight': 4}, {'node': 26, 'weight': 2}], 
                     26: [{'node': 25, 'weight': 2}, {'node': 14, 'weight': 4}, {'node': 27, 'weight': 1}, {'node': 22, 'weight': 5}],
                     27: [{'node': 26, 'weight': 1}, {'node': 15, 'weight': 4}, {'node': 16, 'weight': 4}, {'node': 21, 'weight': 5}]}


grands_axes_paris = nx.Graph()
for node in grands_axes_paris_dict:
    grands_axes_paris.add_node(node)
    for elem in grands_axes_paris_dict[node]:
        grands_axes_paris.add_edge(node, elem['node'], weight=elem['weight'])


adjacency_matrix = nx.to_pandas_adjacency(grands_axes_paris, nonedge=np.inf).round(decimals=0)


def remove_node(graph, node):
    """For networkx graphs"""
    removed_graph = graph.copy()
    removed_graph.remove_node(node)
    return removed_graph


if __name__ == "__main__":
    print(adjacency_matrix)
    plt.figure(figsize=(7, 7))
    nx.draw_networkx(grands_axes_paris)
    plt.draw()
    plt.show()