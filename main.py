from game import *
from graph import adjacency_matrix, grands_axes_paris
from copy import deepcopy
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.text import Annotation
from matplotlib import rcParams
import matplotlib.patches as patches
import networkx as nx
import time

def game_loop(graph, list_src_dst, nb_actions, nb_iterations):
    players = {}
    nb_players = len(list_src_dst)
    V = len(graph)

    # # Classical initialization of the players
    # for i in range(nb_players):
    #     players[i] = regretMatchingPlayer(i, graph, list_src_dst[i][0], list_src_dst[i][1], nb_actions, nb_iterations)
        
    # Manual initialization: here example for 1 src node and 1 dst node
    manual_actions = get_paths(graph, list_src_dst[0][0], list_src_dst[0][1], nb_actions)
    for i in range(nb_players):
        players[i] = regretMatchingPlayer(i, graph, list_src_dst[i][0], list_src_dst[i][1], nb_actions, nb_iterations, manual_actions=manual_actions)


    # Repeated play
    decisions = []
    dynamic_adjacency_tensor = []
    busyness_matrix = adjacency_matrix.apply(lambda x: np.zeros(V,dtype=np.int8))
    sum_utility = np.zeros(nb_iterations)
    for T in range(nb_iterations):
        print("---------------- Iteration",T+1, "------------------")
        ## Compute choices for all players
        decisions.append([players[i].decision(T) for i in range(nb_players)])

        ## Adjust weights depending on the choices
        busyness_matrix = busyness_matrix.apply(lambda x: np.zeros(V,dtype=np.int8))
        for decision_player in decisions[T]:
            for i in range(len(decision_player)-1):
                busyness_matrix[decision_player[i]][decision_player[i+1]] = actualize_busyness_matrix(busyness_matrix[decision_player[i]][decision_player[i+1]])
        
        dynamic_adjacency_matrix = actualize_dynamic_matrix(adjacency_matrix, busyness_matrix, V, nb_players)
        dynamic_adjacency_tensor.append(dynamic_adjacency_matrix)

        ## Update the strategies for the players
        for i in range(nb_players):
            players[i].play(T, dynamic_adjacency_tensor)

        sum_utility[T] = sum([players[i].U[T] for i in range(nb_players)])
        print("Sum of all players' utility:", sum_utility[T],"\n")

    # print(dynamic_adjacency_tensor[0])
    # print(manual_actions)
    return players, decisions, sum_utility


def animation_5_players():
        
    ## Parameters
    graph = grands_axes_paris.copy()
    list_src_dst = [(6,11),(6,11),(6,11),(6,11),(6,11)]
    nb_players = len(list_src_dst)
    nb_actions = 5
    nb_iterations = 30
    players, decisions, sum_utility = game_loop(graph, list_src_dst, nb_actions, nb_iterations)

    fig1 = plt.figure(figsize=(8,8))
    plt.plot(np.arange(1,nb_iterations+1), np.array(sum_utility))
    plt.title("Evolution of the sum of the utilities")
    plt.show()

    ## Animation
    fig = plt.figure(figsize=(8,8))
    ax = plt.axes()
    seed = 31
    pos = nx.spring_layout(grands_axes_paris, seed = seed)
    nx.draw_networkx(grands_axes_paris, pos)
    colors = ["b",'r','g','m','c']
    width = [20,16,12,8,4]

    ims = []
    for t in range(nb_iterations):
        decision_t = decisions[t]
        im = []
        for ID, path in enumerate(decision_t):
            path_edges = [[path[j],path[j+1]] for j in range(len(path)-1)]
            im.append(nx.draw_networkx_edges(grands_axes_paris,pos,edgelist=path_edges,edge_color=colors[ID],width=width[ID]))
        im.append(ax.text(x=0.8,y=0.75,s=f"Iteration {t}"))
        ims.append(im)

    anim = animation.ArtistAnimation(fig, ims, interval=100, blit=True, repeat = False)
    plt.show()


def animation_clusters():
    t0 = time.time()
    graph = grands_axes_paris.copy()
    V = len(graph)
    # list_src_dst = create_cluster(100,1,1,0,V)
    list_src_dst = [(9,25) for i in range(100)]
    nb_players = len(list_src_dst)
    nb_actions = 10
    nb_iterations = 50
    players, decisions, sum_utility = game_loop(graph, list_src_dst, nb_actions, nb_iterations)

    ## Evolution of the sum of the utilities
    fig1 = plt.figure(figsize=(8,8))
    plt.plot(np.arange(1,nb_iterations+1), np.array(sum_utility))
    plt.title("Evolution of the sum of the utilities")

    ## Animation
    fig3 = plt.figure(figsize=(8,8))
    ax3 = plt.axes()
    seed = 31
    pos = nx.spring_layout(grands_axes_paris, seed = seed)

    ims_path = []
    for t in range(nb_iterations):
        print(f"Generating frame {t+1}/{nb_iterations} ...")
        im = []
        path_width_matrix = adjacency_matrix.apply(lambda x: np.ones(V))
        path_color_matrix = adjacency_matrix.apply(lambda x: ["k" for i in range(V)])
        decision_t = decisions[t]
        for path in decision_t:
            path_edges = [(path[j],path[j+1]) for j in range(len(path)-1)]
            for (i,j) in path_edges:
                path_width_matrix[i][j] += 30/nb_players
                path_color_matrix[i][j] = "r"

        im.append(nx.draw_networkx_edges(grands_axes_paris,pos))
        for i in range(1, V+1):
            for j in range(1, V+1):
                if path_width_matrix[i][j] > 1:
                    im.append(nx.draw_networkx_edges(grands_axes_paris,pos,edgelist=[(i,j)],edge_color=path_color_matrix[i][j],width=path_width_matrix[i][j]))
        im.append(nx.draw_networkx_nodes(grands_axes_paris, pos))
        im.append(nx.draw_networkx_nodes(grands_axes_paris, pos, nodelist = [decision_t[0][0], decision_t[0][-1]], node_color = "g"))
        labels = nx.draw_networkx_labels(grands_axes_paris, pos)
        for i in list(labels.keys()):
            im.append(labels[i])
        im.append(ax3.text(x=0.8,y=0.75,s=f"Iteration {t+1}"))
        im.append(ax3.text(x=-0.75,y=-1,s=f"Sum of utilities: {sum_utility[t]}"))
        ims_path.append(im)

    anim_path = animation.ArtistAnimation(fig3, ims_path, interval=700, blit=True, repeat = True)
    ax3.set_title("Evolution of the congestions")

    ## Evolution of the probabilities
    fig2 = plt.figure(figsize=(8,8))
    ax2 = plt.axes()
    sigma = [players[i].sigma for i in range(nb_players)]
    
    barcollection = ax2.bar(x=np.arange(0,nb_actions), height=[0 for i in range(nb_actions)])
    def animate(t):
        sigma_t = np.array([[sigma[i][t][j] for j in range(nb_actions)] for i in range(nb_players)])
        means = np.mean(sigma_t,axis=0)
        dist = np.abs(sigma_t - means)
        mean_dist = np.mean(dist)
        std_dist = np.std(dist)
        for i, b in enumerate(barcollection):
            b.set_height(means[i])
        plt.title(f"Evolution of the strategies \nIteration {t} \nMean distance = {mean_dist:.2g} \nStd distance = {std_dist:.2g}")

    anim_prob = animation.FuncAnimation(fig2, animate, interval=700, repeat = True, frames=nb_iterations-1)
    ax2.set_ylim([0,1])
    ax2.set_xlabel("Paths")
    ax2.set_ylabel("Probabilities")


    anim_prob.save(f'Probabilities {nb_players} players {nb_iterations} iterations.gif', writer='Pillow', fps=2)
    anim_path.save(f'Paths {nb_players} players {nb_iterations} iterations.gif', writer='Pillow', fps=2)
    t1 = time.time()
    print(f"Time elapsed: {t1-t0:.2g}s")
    plt.show()


if __name__ == "__main__":
    # animation_5_players()
    animation_clusters()
